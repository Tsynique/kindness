<?php

namespace AntiKorona\Kindness\Classes;

use Auth;
use BackendAuth;
use RainLab\Translate\Classes\Translator;
use RainLab\Translate\Models\Locale;

class CommonHelper {
    public static function backendUser() {
        if (!BackendAuth::check()) {
            return null;
        }

        $user = BackendAuth::getUser();

        return $user;
    }

    public static function backendUserOrFail() {
        $user = self::backendUser();
        if ($user === null) {
            die;
        }

        return $user;
    }

    public static function user() {
        if (!Auth::check()) {
            return null;
        }

        $user = Auth::getUser();

        return $user;
    }

    public static function userOrFail() {
        $user = self::user();
        if ($user === null) {
            die;
        }

        return $user;
    }

    public static function getLocale(): string {
        $translator = Translator::instance();
        return $translator->getLocale();
    }

    public static function getAllActiveLocales(): array {
        return Locale::listEnabled();
    }
}
