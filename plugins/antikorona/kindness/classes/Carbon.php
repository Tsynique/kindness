<?php

namespace AntiKorona\Kindness\Classes;

use Carbon\Carbon as BaseCarbon;

class Carbon extends BaseCarbon {
    const SECONDS_PER_WEEK = 604800;
    const SECONDS_PER_DAY = 86400;

    public function __construct($time = null, $tz = 'UTC') {
        parent::__construct($time, $tz);
    }

    public static function createFromFormat($format, $time, $tz = 'UTC') {
        return parent::createFromFormat($format, $time, $tz);
    }

    public function midnight() {
        $this->setTime(0, 0, 0, 0);
    }
}
