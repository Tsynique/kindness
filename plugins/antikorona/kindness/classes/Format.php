<?php

namespace AntiKorona\Kindness\Classes;

class Format {
    public const DATE_FORMAT = 'Y-m-d';
    public const DATETIME_FULL_FORMAT = 'Y-m-d H:i:s';

    public static function getDateFormat(): string {
        return self::DATE_FORMAT;
    }

    public static function getDateTimeFormat(): string {
        return self::DATETIME_FORMAT;
    }

    public static function getDateTimeFullFormat(): string {
        return self::DATETIME_FULL_FORMAT;
    }

    public static function date(int $timestamp = null): string {
        if ($timestamp === null) {
            $timestamp = time();
        }

        return date(self::getDateFormat(), $timestamp);
    }

    public static function number(float $number, int $decimals = 2): string {
        return number_format($number, $decimals, '.', '');
    }
}
