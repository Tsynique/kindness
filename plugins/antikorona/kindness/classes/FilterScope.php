<?php

namespace AntiKorona\Kindness\Classes;

use Backend\Classes\FilterScope as FilterScopeBase;

class FilterScope extends FilterScopeBase {
    public $nameFromSelect = '';

    public $textFilterOptions = [];

    protected function evalConfig($config) {
        if ($config === null) {
            $config = [];
        }

        /*
         * Standard config:property values
         */
        $applyConfigValues = [
            'options',
            'dependsOn',
            'context',
            'default',
            'conditions',
            'scope',
            'cssClass',
            'nameFrom',
            'nameFromSelect',
            'descriptionFrom',
            'disabled',
            'textFilterOptions',
        ];

        foreach ($applyConfigValues as $value) {
            if (array_key_exists($value, $config)) {
                $this->{$value} = $config[$value];
            }
        }

        return $config;
    }

    public function getTextFilterFields(): array {
        if (empty($this->textFilterOptions['fields'])) {
            return [];
        }

        return $this->textFilterOptions['fields'];
    }
}
