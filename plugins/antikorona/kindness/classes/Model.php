<?php

namespace AntiKorona\Kindness\Classes;

use AntiKorona\Kindness\Classes\Carbon;
use October\Rain\Database\Model as OctoberModel;

class Model extends OctoberModel {
    public function convertDateToTimestamp(string $field) {
        if (strlen((string)(int)$this->{$field}) === 10) {
            return;
        }

        $date = Carbon::createFromFormat(Format::getDateFormat(), $this->{$field}, 'UTC');
        $date->setTime(0, 0, 0);
        $this->{$field} = $date->timestamp;
    }

    public function convertDateTimeFullToTimestamp(string $field): void {
        if (strlen((string)(int)$this->{$field}) === 10) {
            return;
        }

        $dateTime = Carbon::createFromFormat(Format::getDateTimeFullFormat(), $this->{$field}, 'UTC');
        $this->{$field} = $dateTime->timestamp;
    }
}
