<?php

namespace AntiKorona\Kindness\Widgets;

use Carbon\Carbon;
use AntiKorona\Kindness\Classes\Carbon as KindnessCarbon;
use AntiKorona\Kindness\Classes\FilterScope;
use Backend\Widgets\Filter as FilterBase;
use Db;
use DbDongle;

class Filter extends FilterBase {

    protected function makeFilterScope($name, $config) {
        $label = $config['label'] ?? null;
        $scopeType = $config['type'] ?? null;

        $scope = new FilterScope($name, $label);
        $scope->displayAs($scopeType, $config);
        $scope->idPrefix = $this->alias;

        /*
         * Set scope value
         */
        $scope->value = $this->getScopeValue($scope, @$config['default']);

        return $scope;
    }

    public function getScopeNameFromSelect($scope) {
        if (is_string($scope)) {
            $scope = $this->getScope($scope);
        }

        return $scope->nameFromSelect;
    }

    protected function getOptionsFromModel($scope, $searchQuery = null) {
        $model = $this->scopeModels[$scope->scopeName];

        $query = $model->newQuery();

        /*
         * The 'group' scope has trouble supporting more than 500 records at a time
         * @todo Introduce a more advanced version with robust list support.
         */
        $query->limit(500);
        $nameFromSelect = $this->getScopeNameFromSelect($scope);
        if (!empty($nameFromSelect)) {
            $query->select();
            $query->addSelect([Db::raw($nameFromSelect)]);
        }
        $this->fireSystemEvent('backend.filter.extendQuery', [$query, $scope]);

        if (!$searchQuery) {
            return $query->get();
        }

        $searchFields = [$model->getKeyName(), $this->getScopeNameFrom($scope)];
        return $query->searchWhere($searchQuery, $searchFields)->get();
    }

    public function applyScopeToQuery($scope, $query) {
        if (is_string($scope)) {
            $scope = $this->getScope($scope);
        }

        if (!$scope->value) {
            return;
        }

        switch ($scope->type) {
            case 'date':
                if ($scope->value instanceof Carbon) {
                    $value = KindnessCarbon::createFromTimestamp($scope->value->timestamp, 'UTC');

                    /*
                     * Condition
                     */
                    if ($scopeConditions = $scope->conditions) {
                        $query->whereRaw(DbDongle::parse(strtr($scopeConditions, [
                                ':filtered' => $value->format('Y-m-d'),
                                ':after' => $value->format('Y-m-d H:i:s'),
                                ':before' => $value->copy()->addDay()->addMinutes(-1)->format('Y-m-d H:i:s')
                        ])));
                    }
                    /*
                     * Scope
                     */ elseif ($scopeMethod = $scope->scope) {
                        $query->$scopeMethod($value);
                    }
                }

                break;

            case 'daterange':
                if (is_array($scope->value) && count($scope->value) > 1) {
                    list($after, $before) = array_values($scope->value);

                    if ($after && $after instanceof Carbon && $before && $before instanceof Carbon) {
                        /*
                         * Condition
                         */
                        if ($scopeConditions = $scope->conditions) {
                            $query->whereRaw(DbDongle::parse(strtr($scopeConditions, [
                                    ':afterDate' => $after->format('Y-m-d'),
                                    ':after' => $after->format('Y-m-d H:i:s'),
                                    ':beforeDate' => $before->format('Y-m-d'),
                                    ':before' => $before->format('Y-m-d H:i:s')
                            ])));
                        }
                        /*
                         * Scope
                         */ elseif ($scopeMethod = $scope->scope) {
                            $query->$scopeMethod($after, $before);
                        }
                    }
                }

                break;

            case 'number':
                if (is_numeric($scope->value)) {
                    /*
                     * Condition
                     */
                    if ($scopeConditions = $scope->conditions) {
                        $query->whereRaw(DbDongle::parse(strtr($scopeConditions, [
                                ':filtered' => $scope->value,
                        ])));
                    }
                    /*
                     * Scope
                     */ elseif ($scopeMethod = $scope->scope) {
                        $query->$scopeMethod($scope->value);
                    }
                }

                break;

            case 'numberrange':
                if (is_array($scope->value) && count($scope->value) > 1) {
                    list($min, $max) = array_values($scope->value);

                    if (isset($min) || isset($max)) {
                        /*
                         * Condition
                         */
                        if ($scopeConditions = $scope->conditions) {
                            $query->whereRaw(DbDongle::parse(strtr($scopeConditions, [
                                    ':min' => $min === null ? -2147483647 : $min,
                                    ':max' => $max === null ? 2147483647 : $max
                            ])));
                        }
                        /*
                         * Scope
                         */ elseif ($scopeMethod = $scope->scope) {
                            $query->$scopeMethod($min, $max);
                        }
                    }
                }

                break;

            case 'text':
                $this->applyTextScopeToQuery($scope, $query);
                break;

            default:
                $value = is_array($scope->value) ? array_keys($scope->value) : $scope->value;

                if (empty($value)) {
                    break;
                }

                /*
                 * Condition
                 */
                if ($scopeConditions = $scope->conditions) {
                    /*
                     * Switch scope: multiple conditions, value either 1 or 2
                     */
                    if (is_array($scopeConditions)) {
                        $conditionNum = is_array($value) ? 0 : $value - 1;
                        list($scopeConditions) = array_slice($scopeConditions, $conditionNum);
                    }

                    if (is_array($value)) {
                        $filtered = implode(',', array_build($value, function ($key, $_value) {
                                return [$key, Db::getPdo()->quote($_value)];
                            }));
                    } else {
                        $filtered = Db::getPdo()->quote($value);
                    }

                    $query->whereRaw(DbDongle::parse(strtr($scopeConditions, [':filtered' => $filtered])));
                }
                /*
                 * Scope
                 */ elseif ($scopeMethod = $scope->scope) {
                    $query->$scopeMethod($value);
                }

                break;
        }

        return $query;
    }

    public function applyTextScopeToQuery(FilterScope $scope, &$query) {
        if ($scopeConditions = $scope->conditions) {
            $query->whereRaw(DbDongle::parse(strtr($scopeConditions, [
                    ':value' => Db::getPdo()->quote($scope->value),
            ])));

        } elseif (!empty($scope->scope) && $scope->scope === 'searchWhere') {
            $fields = $scope->getTextFilterFields();
            if (empty($fields)) {
                $query->searchWhere($scope->value, [$scope->scopeName]);
            } else {
                $query->where(function($query) use ($fields, $scope) {
                    foreach ($fields as $i => $field) {
                        if ($i === 0) {
                            $query->searchWhere($scope->value, $field);
                        } else {
                            $query->orSearchWhere($scope->value, $field);
                        }
                    }
                });
            }
        }

        /*
         * Scope
         */ elseif ($scopeMethod = $scope->scope) {
            if ($scopeMethod === 'searchWhere') {
                $query->searchWhere($scope->value, [$scope->scopeName]);
            } else {
                $query->$scopeMethod($scope->value);
            }
        }

//        die($query->toSql());
    }
}
