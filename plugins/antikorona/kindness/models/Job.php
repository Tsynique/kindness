<?php

namespace AntiKorona\Kindness\Models;

use AntiKorona\Kindness\Classes\CommonHelper;
use AntiKorona\Kindness\Classes\Model;
use October\Rain\Database\Traits\SoftDelete;
use October\Rain\Database\Traits\Validation;
use ValidationException;

class Job extends Model {
    use Validation;
    use SoftDelete;

    const STATUS_DRAFT = 'draft';
    const STATUS_READY = 'ready';
    const STATUS_RESERVED = 'reserved';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_COMPLETED = 'completed';
    const STATUS_FAILED = 'failed';
    const STATUS_CLOSED = 'closed';

    const PRIORITY_1 = 1;
    const PRIORITY_2 = 2;
    const PRIORITY_3 = 3;
    const PRIORITY_4 = 4;
    const PRIORITY_5 = 5;

    protected $dates = ['deleted_at'];

    public $timestamps = false;

    public $table = 'antikorona_kindness_job';

    public $rules = [
        'volunteers_needed' => 'numeric|min:1',
    ];

    public $jsonable = [
        'volunteers_json',
    ];

    public $belongsTo = [
        'volunteer' => [
            Volunteer::class,
        ],
        'user' => [
            \Backend\Models\User::class,
            'key' => 'created_by',
        ],
    ];

    public $belongsToMany = [
        'competence' => [
            Competence::class,
            'table' => 'antikorona_kindness_competence2job',
        ],
        'volunteers' => [
            Volunteer::class,
            'table' => 'antikorona_kindness_volunteer2job',
        ],
    ];

    public function beforeSave() {
        $this->validateVoluteers();

        if (!empty($this->deadline_at)) {
            $this->convertDateTimeFullToTimestamp('deadline_at');
        }

        if (!empty($this->completed_at)) {
            $this->convertDateTimeFullToTimestamp('completed_at');
        }

        if (empty($this->status)) {
            $this->status = self::STATUS_DRAFT;
        }

        if (empty($this->created_by)) {
            $user = CommonHelper::backendUserOrFail();
            $this->created_by = $user->id;
        }
    }

    private function validateVoluteers() {
        $volunteers = $this->volunteers_json;
        if (empty($volunteers)) {
            return;
        }

        $volunteers_needed = (int)$this->volunteers_needed;
        $volunteer_ids = array_column($volunteers, 'volunteer');

        if (count($volunteer_ids) < 1) {
            return;
        }

        if (count($volunteer_ids) !== count(array_unique($volunteer_ids))) {
            $msg = 'Negalima priskirti to pačio savanorio kelis kartus!';
            throw new ValidationException(['volunteers_json' => $msg]);
        }

        if (count($volunteer_ids) > $volunteers_needed) {
            $msg = 'Viršytas savanorių kiekis šioje užduotyje!';
            throw new ValidationException(['volunteers_json' => $msg]);
        }
    }

    public static function getStatusKeys(): array {
        return array_keys(self::getStatuses());
    }

    public static function getStatuses(): array {
        return [
            self::STATUS_DRAFT => 'Ruošiama',
            self::STATUS_READY => 'Nepradėta',
            self::STATUS_RESERVED => 'Rezervuota',
            self::STATUS_IN_PROGRESS => 'Vykdoma',
            self::STATUS_COMPLETED => 'Atlikta',
            self::STATUS_FAILED => 'Nepavyko atlikti',
            self::STATUS_CLOSED => 'Uždaryta',
        ];
    }

    public function getStatusFormatted(): string {
        $statuses = self::getStatuses();

        if (isset($statuses[$this->status])) {
            return $statuses[$this->status];
        }

        return (string)$this->status;
    }

    public static function getPriorities(): array {
        return [
            self::PRIORITY_1 => 'Labai neskubus',
            self::PRIORITY_2 => 'Neskubus',
            self::PRIORITY_3 => 'Vidutiniškai skubus',
            self::PRIORITY_4 => 'Skubus',
            self::PRIORITY_5 => 'Labai skubus',
        ];
    }

    public function getPriorityFormatted(): string {
        $statuses = self::getPriorities();

        if (isset($statuses[$this->priority])) {
            return $statuses[$this->priority];
        }

        return (string)$this->priority;
    }

    public function getPossibleStatuses(): array {
        $statusAutomata = [
            self::STATUS_DRAFT => [
                self::STATUS_READY,
            ],
            self::STATUS_READY => [
                self::STATUS_DRAFT,
                self::STATUS_RESERVED,
                self::STATUS_IN_PROGRESS,
            ],
            self::STATUS_RESERVED => [
                self::STATUS_READY,
                self::STATUS_IN_PROGRESS,
            ],
            self::STATUS_IN_PROGRESS => [
                self::STATUS_READY,
                self::STATUS_COMPLETED,
                self::STATUS_FAILED,
            ],
            self::STATUS_COMPLETED => [
                self::STATUS_READY,
                self::STATUS_CLOSED,
            ],
            self::STATUS_FAILED => [
                self::STATUS_READY,
                self::STATUS_CLOSED,
            ],
            self::STATUS_CLOSED => [],
        ];

        $status = $this->status;
        $statuses = self::getStatuses();
        if (isset($statusAutomata[$status])) {
            $possible = $statusAutomata[$status];
            $possibleStatuses = [];
            foreach ($possible as $statusId) {
                $possibleStatuses[$statusId] = $statuses[$statusId];
            }

            return $possibleStatuses;
        }

        return [];
    }

    public function scopeFieldFilter($query, $val) {
        return $query->like('city', '%'.$val.'%');
    }

    public function isMine(): bool {
        $backendUser = CommonHelper::backendUserOrFail();
        return ($this->user_id === $backendUser->id);
    }
}
