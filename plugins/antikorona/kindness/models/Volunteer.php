<?php

namespace AntiKorona\Kindness\Models;

use Model;

class Volunteer extends Model {

    use \October\Rain\Database\Traits\Validation;

    public $timestamps = false;

    public $table = 'antikorona_kindness_volunteer';

    public $rules = [
    ];

    public $belongsToMany = [
        'competences' => [
            Competence::class,
            'table' => 'antikorona_kindness_competence2volunteer',
        ]
    ];
}
