<?php

namespace AntiKorona\Kindness\Models;

use Model;

class Competence extends Model {

    use \October\Rain\Database\Traits\Validation;

    public $timestamps = false;

    public $table = 'antikorona_kindness_competence';

    public $rules = [
    ];

    public $fillable = [
        'title',
    ];
}
