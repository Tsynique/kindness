<?php

namespace AntiKorona\Kindness\Behaviors;

use Backend\Behaviors\ListController as ListControllerBase;

class ListController extends ListControllerBase {
    public function makeWidget($class, $widgetConfig = []) {
        $overrideWidgets = $this->controller->overrideWidgets;
        if (!empty($overrideWidgets) && in_array($class, $overrideWidgets)) {
            return $this->controller->makeWidget($class, $widgetConfig);
        }

        return parent::makeWidget($class, $widgetConfig);
    }
}
