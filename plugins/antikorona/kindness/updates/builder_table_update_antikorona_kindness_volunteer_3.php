<?php namespace AntiKorona\Kindness\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAntikoronaKindnessVolunteer3 extends Migration
{
    public function up()
    {
        Schema::table('antikorona_kindness_volunteer', function($table)
        {
            $table->integer('is_trained')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('antikorona_kindness_volunteer', function($table)
        {
            $table->dropColumn('is_trained');
        });
    }
}
