<?php namespace AntiKorona\Kindness\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAntikoronaKindnessJob extends Migration
{
    public function up()
    {
        Schema::table('antikorona_kindness_job', function($table)
        {
            $table->integer('started_at')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('antikorona_kindness_job', function($table)
        {
            $table->dropColumn('started_at');
        });
    }
}
