<?php namespace AntiKorona\Kindness\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAntikoronaKindnessVolunteer2 extends Migration
{
    public function up()
    {
        Schema::table('antikorona_kindness_volunteer', function($table)
        {
            $table->text('municipality')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('antikorona_kindness_volunteer', function($table)
        {
            $table->dropColumn('municipality');
        });
    }
}
