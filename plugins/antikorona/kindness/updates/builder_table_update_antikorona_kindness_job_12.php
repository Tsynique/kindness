<?php namespace AntiKorona\Kindness\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAntikoronaKindnessJob12 extends Migration
{
    public function up()
    {
        Schema::table('antikorona_kindness_job', function($table)
        {
            $table->integer('priority')->default(3);
        });
    }
    
    public function down()
    {
        Schema::table('antikorona_kindness_job', function($table)
        {
            $table->dropColumn('priority');
        });
    }
}
