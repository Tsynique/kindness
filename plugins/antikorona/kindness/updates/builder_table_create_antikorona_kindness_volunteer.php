<?php namespace AntiKorona\Kindness\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAntikoronaKindnessVolunteer extends Migration
{
    public function up()
    {
        Schema::create('antikorona_kindness_volunteer', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('firstname', 100)->nullable();
            $table->string('lastname', 100)->nullable();
            $table->string('phone', 100)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('antikorona_kindness_volunteer');
    }
}
