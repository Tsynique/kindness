<?php namespace AntiKorona\Kindness\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAntikoronaKindnessCompetence2volunteer extends Migration
{
    public function up()
    {
        Schema::create('antikorona_kindness_competence2volunteer', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('competence_id')->unsigned();
            $table->integer('volunteer_id')->unsigned();
            $table->primary(['competence_id','volunteer_id'], 'comp_vol_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('antikorona_kindness_competence2volunteer');
    }
}
