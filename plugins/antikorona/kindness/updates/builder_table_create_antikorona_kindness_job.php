<?php namespace AntiKorona\Kindness\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAntikoronaKindnessJob extends Migration
{
    public function up()
    {
        Schema::create('antikorona_kindness_job', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('deleted_at')->nullable();
            $table->string('title', 256)->nullable();
            $table->text('description')->nullable();
            $table->string('beneficiary_firstname', 256)->nullable();
            $table->string('beneficiary_lastname', 256)->nullable();
            $table->string('beneficiary_company', 256)->nullable();
            $table->text('beneficiary_comment')->nullable();
            $table->integer('volunteer_id')->nullable()->unsigned();
            $table->integer('deadline_at')->nullable()->unsigned();
            $table->integer('completed_at')->nullable()->unsigned();
            $table->string('status', 32)->nullable();
            $table->text('completion_comment')->nullable();
            $table->text('volunteer_comment')->nullable();
            $table->string('municipality', 32)->nullable();
            $table->string('city', 32)->nullable();
            $table->string('address', 32)->nullable();
            $table->string('address_other', 1000)->nullable();
            $table->text('geo_coordinates')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('antikorona_kindness_job');
    }
}
