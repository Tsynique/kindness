<?php namespace AntiKorona\Kindness\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAntikoronaKindnessJob2 extends Migration
{
    public function up()
    {
        Schema::table('antikorona_kindness_job', function($table)
        {
            $table->dateTime('deadline_at')->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('antikorona_kindness_job', function($table)
        {
            $table->integer('deadline_at')->nullable()->unsigned()->default(null)->change();
        });
    }
}
