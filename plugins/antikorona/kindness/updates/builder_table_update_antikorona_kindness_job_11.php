<?php namespace AntiKorona\Kindness\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAntikoronaKindnessJob11 extends Migration
{
    public function up()
    {
        Schema::table('antikorona_kindness_job', function($table)
        {
            $table->integer('volunteers_needed')->unsigned()->default(1);
        });
    }
    
    public function down()
    {
        Schema::table('antikorona_kindness_job', function($table)
        {
            $table->dropColumn('volunteers_needed');
        });
    }
}
