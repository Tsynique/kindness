<?php namespace AntiKorona\Kindness\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAntikoronaKindnessJob13 extends Migration
{
    public function up()
    {
        Schema::table('antikorona_kindness_job', function($table)
        {
            $table->text('sponsor')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('antikorona_kindness_job', function($table)
        {
            $table->dropColumn('sponsor');
        });
    }
}
