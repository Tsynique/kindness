<?php namespace AntiKorona\Kindness\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAntikoronaKindnessCompetence extends Migration
{
    public function up()
    {
        Schema::create('antikorona_kindness_competence', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title', 100);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('antikorona_kindness_competence');
    }
}
