<?php namespace AntiKorona\Kindness\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAntikoronaKindnessVolunteer extends Migration
{
    public function up()
    {
        Schema::table('antikorona_kindness_volunteer', function($table)
        {
            $table->string('email', 100)->nullable();
            $table->string('address', 100)->nullable();
            $table->string('city', 100)->nullable();
            $table->integer('agreement_signed')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('antikorona_kindness_volunteer', function($table)
        {
            $table->dropColumn('email');
            $table->dropColumn('address');
            $table->dropColumn('city');
            $table->dropColumn('agreement_signed');
        });
    }
}
