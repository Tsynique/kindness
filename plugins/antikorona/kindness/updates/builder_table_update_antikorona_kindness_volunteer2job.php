<?php namespace AntiKorona\Kindness\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAntikoronaKindnessVolunteer2job extends Migration
{
    public function up()
    {
        Schema::table('antikorona_kindness_volunteer2job', function($table)
        {
            $table->dropPrimary(['volunteer_id','job_id']);
            $table->integer('id');
            $table->integer('volunteer_id')->unsigned(false)->change();
            $table->integer('job_id')->unsigned(false)->change();
            $table->primary(['id']);
        });
    }
    
    public function down()
    {
        Schema::table('antikorona_kindness_volunteer2job', function($table)
        {
            $table->dropPrimary(['id']);
            $table->dropColumn('id');
            $table->integer('volunteer_id')->unsigned()->change();
            $table->integer('job_id')->unsigned()->change();
            $table->primary(['volunteer_id','job_id']);
        });
    }
}
