<?php namespace AntiKorona\Kindness\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAntikoronaKindnessVolunteer2job extends Migration
{
    public function up()
    {
        Schema::create('antikorona_kindness_volunteer2job', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('volunteer_id')->unsigned();
            $table->integer('job_id')->unsigned();
            $table->primary(['volunteer_id','job_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('antikorona_kindness_volunteer2job');
    }
}
