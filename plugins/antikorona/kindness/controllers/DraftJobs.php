<?php

namespace AntiKorona\Kindness\Controllers;

use AntiKorona\Kindness\Models\Job;
use BackendMenu;
use October\Rain\Database\Builder;

class DraftJobs extends Jobs {
    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('AntiKorona.Kindness', 'jobs', 'draftjobs');
    }

    public function listExtendQuery(Builder $query, $definition = null) {
        parent::listExtendQuery($query, $definition);

        $query->where('status', Job::STATUS_DRAFT);
    }
}
