<?php

namespace AntiKorona\Kindness\Controllers;

use AntiKorona\Kindness\Models\Job;
use BackendMenu;
use October\Rain\Database\Builder;

class ClosedJobs extends Jobs {
    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('AntiKorona.Kindness', 'jobs', 'closejobs');
    }

    public function listExtendQuery(Builder $query, $definition = null) {
        parent::listExtendQuery($query, $definition);

        $query->where('status', Job::STATUS_CLOSED);
    }
}
