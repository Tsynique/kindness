<?php

namespace AntiKorona\Kindness\Controllers;

use AntiKorona\Kindness\Models\Job;
use BackendMenu;
use October\Rain\Database\Builder;

class ReadyJobs extends Jobs {

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('AntiKorona.Kindness', 'jobs', 'readyjobs');
    }

    public function listExtendQuery(Builder $query, $definition = null) {
        parent::listExtendQuery($query, $definition);

        $query->where(function($query) {
            $query->where('status', Job::STATUS_READY)
                ->orwhere('status', Job::STATUS_RESERVED);
        });
    }

}
