<?php

namespace AntiKorona\Kindness\Controllers;

use AntiKorona\Kindness\Classes\CommonHelper;
use AntiKorona\Kindness\Models\Job;
use AntiKorona\Kindness\Widgets\Filter;
use Backend;
use Backend\Classes\Controller;
use BackendMenu;
use Db;
use Flash;
use Redirect;
use October\Rain\Database\Builder;

class Jobs extends Controller {
    public $implement = [
        'AntiKorona\Kindness\Behaviors\ListController',
        'Backend\Behaviors\FormController',
    ];
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $overrideWidgets = [
        'Backend\Widgets\Filter',
    ];

    public function __construct() {
        BackendMenu::setContext('AntiKorona.Kindness', 'jobs');
        parent::__construct();
        $this->addCss("/plugins/antikorona/kindness/assets/css/jobs.css");
    }

    public function makeWidget($class, $widgetConfig = []) {
        $class = Filter::class;
        return new $class($this, $widgetConfig);
    }

    public function listInjectRowClass($record, $definition = null): string {
        return 'priority-'.$record->priority;
    }

    public function listExtendQuery(Builder $query, $definition = null) {
        $addressFull = 'CONCAT(
            address, \' \',
            address_other, \' \',
            city
        )';
        $query->addSelect(Db::raw($addressFull.' AS address_full'));
        $beneficiaryFull = 'CONCAT(
            COALESCE(beneficiary_firstname, \'\'), \' \',
            COALESCE(beneficiary_phone, \'\')
        )';
        $query->addSelect(Db::raw($beneficiaryFull.' AS beneficiary_full'));

        $backendUser = CommonHelper::backendUserOrFail();
        $query->where(function($query) use ($backendUser) {
            $query->where('status', '!=', Job::STATUS_DRAFT)
            ->orwhere('created_by', $backendUser->id);
        });

        $filterStatus = input('status');
        if (!empty($filterStatus) && in_array($filterStatus, Job::getStatusKeys())) {
            $query->where('status', $filterStatus);
        }
    }

    public function onStatusChange() {
        $id = input('id');
        $newStatus = input('status');
        $job = Job::findOrFail($id);
        $possibleStatuses = $job->getPossibleStatuses();
        if (!isset($possibleStatuses[$newStatus])) {
            Flash::error('Būsenos keitimas negalimas!');
            return;
        }

        $job->status = $newStatus;
        $job->forceSave();
        Flash::success('Būsena pakeista!');
        $statusSimple = str_replace('_', '', $newStatus);
        if ($statusSimple === 'reserved') {
            $statusSimple = 'ready';
        }
        $redirectUrl = Backend::url('antikorona/kindness/'.$statusSimple.'jobs');
        return Redirect::to($redirectUrl);
    }
}
