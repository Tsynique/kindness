<?php

namespace AntiKorona\Kindness\Controllers;

use BackendMenu;
use October\Rain\Database\Builder;

class DeletedJobs extends Jobs {
    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('AntiKorona.Kindness', 'jobs', 'deletedjobs');
    }

    public function listExtendQuery(Builder $query, $definition = null) {
        parent::listExtendQuery($query, $definition);

        $query->withTrashed();
        $query->whereNotNull('deleted_at');
    }

    /**
     * Extend the query used for finding the form model. Extra conditions
     * can be applied to the query, for example, $query->withTrashed();
     * @param October\Rain\Database\Builder $query
     * @return void
     */
    public function formExtendQuery($query) {
        parent::formExtendQuery($query);

        $query->withTrashed();
        $query->whereNotNull('deleted_at');
    }
}
