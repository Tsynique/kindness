<?php

namespace AntiKorona\Kindness\Controllers;

use AntiKorona\Kindness\Widgets\Filter;
use Backend\Classes\Controller;
use BackendMenu;

class Volunteers extends Controller {
    public $implement = [
        'AntiKorona\Kindness\Behaviors\ListController',
        'Backend\Behaviors\FormController',
    ];
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $overrideWidgets = [
        'Backend\Widgets\Filter',
    ];

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('AntiKorona.Kindness', 'volunteers');
    }

    public function makeWidget($class, $widgetConfig = []) {
        $class = Filter::class;
        return new $class($this, $widgetConfig);
    }
}
