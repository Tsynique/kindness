var mapsUpdateTimer = null;
function inicializar(x, y, zoom, index, mapDiv, custom)
{
    let latlng = new google.maps.LatLng(x, y);

    let myOptions = {
        zoom: zoom,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    let map = new google.maps.Map(document.getElementById(mapDiv + '-div'), myOptions);

    if (custom) {
        latlng = new google.maps.LatLng(x, y);
    } else {
        latlng = new google.maps.LatLng(0, 0);
    }

    var marker = new google.maps.Marker({
        position: latlng,        
        map: map
    });

    google.maps.event.addListener(map, 'click', function(event) {
        placeMarker(mapDiv, marker, event.latLng);
    });
    
    const geocoder = new google.maps.Geocoder;

    $(".map-input input").on('keyup', function(e) {
        clearTimeout(mapsUpdateTimer);
        setTimeout(function() {
            let address = '';
            let locationInputs = $(".map-input input");
            for (let i = 0; i < locationInputs.length; i++) {
                address += locationInputs[i].value + ' ';
            }

            geocoder.geocode(
                {
                    'address': address
                },
                function(results, status) {

                    if (status == google.maps.GeocoderStatus.OK &&
                        status != google.maps.GeocoderStatus.ZERO_RESULTS
                    ) {
                        map.setCenter(results[0].geometry.location);
                        marker.setPosition(results[0].geometry.location);
                        marker.setVisible(true);
                        let setMapPosition = [marker.getPosition().lat(), marker.getPosition().lng()];
                        $('#' + mapDiv).val(setMapPosition.join(','));
                    }
                }
            );
        }, 1500);
    });
}


function placeMarker(mapDiv, marker, location)
{
    marker.setPosition(location);
    var setMapPosition = [marker.getPosition().lat(), marker.getPosition().lng()];
    $('#' + mapDiv).val(setMapPosition.join(','));
}